## 安装模块

```
pip install -r requirements.txt
```

## 执行代码

```
python train.py
```

## 数据说明

**train.txt:**

- Criteo数据集片段，2000条样本
- 字段：数值特征：I1-13, 类别特征：C14-C39

## AFM论文链接

[Attentional Factorization Machines: Learning the Weight of Feature Interactions via Attention Networks | IJCAI](https://www.ijcai.org/proceedings/2017/435)
## AFM源码链接

https://github.com/jc-LeeHub/Recommend-System-tf2.0/tree/master/AFM

## 关于论文的小总结

[AFM(Attentional Factorization Machines) - 简书 (jianshu.com)](https://www.jianshu.com/p/0a50bf824a4e)

