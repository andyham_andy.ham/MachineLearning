
# MachineLearning-RS

## Introduction
此仓库用于记录在学习推荐系统过程中的知识产出，主要是对经典推荐算法的原理解析及代码实现。



#### About


#### Main contributors(welcome to join us!)


1.  https://gitee.com/andyham_andy.ham
2.  [nlp-tutorial: Natural Language Processing Tutorial for Deep Learning Researchers (github.com)](https://github.com/andyhamgh/nlp-tutorial)
3. [Transformer的PyTorch实现 - mathor (wmathor.com)](https://wmathor.com/index.php/archives/1455/)



#### Reference

1.  https://zhuanlan.zhihu.com/p/53231955
2.  https://www.zhihu.com/column/c_1330637706267734016
3.  https://github.com/shenweichen/deepctr
4.  https://github.com/jc-LeeHub/Recommend-System-tf2.0
